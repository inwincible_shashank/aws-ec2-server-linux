![Logo](logo.jpg)

## Project Title
This repository provides a comprehensive guide to setting up an AWS EC2 server, creating an S3 bucket, installing a Linux operating system, and managing, accessing project files on the server.

## Authors
[@inwincible_shashank](https://gitlab.com/inwincible_shashank/)

## Purpose
To educate fellow QA's about AWS EC2 server so that they can deploy their automation projects and demonstrate their devops practical understanding

## Prerequisites
- AWS free-tier account
- Basic understanding of Linux commands [Learn this from my gitlab project](https://gitlab.com/inwincible_shashank/linuxautomationcmds)

## Download and Installation
1. Download & Install gitbash from here - https://www.git-scm.com/download/win
2. Under Standalone Installer
3. Click on "64-bit Git for Windows Setup" link
4. .exe file will be downloaded
5. Then install the same on your windows machine
6. Then in your project explorer create a new empty folder
7. Left click and open gitbash here
8. Execute below commands
         
```bash
  git clone https://gitlab.com/inwincible_shashank/aws-ec2-server-linux.git
```

## Project Explanation
1. README.md file is the start point for the download, installation and followed by git clone of the repo
2. Then start learning from [Quick Start](#quick-start)  

## Cheat Sheet
For quick interview preparation/ revision use the file [AWS-CheatSheet](AWS-CheatSheet.md)

## Quick Start
To get started quickly, follow below chapters:
1. [Chapter 1: Set up your EC2 server with Linux O.S.](Chapters/Chapter1-EC2-Setup.md)
2. [Chapter 2: Understanding Linux Directory](Chapters/Chapter2-LinuxDirUnderstanding.md)
3. [Chapter 3: Connect EC2 server using MobaXterm](Chapters/Chapter3-MobaXterm.md)
4. [Chapter 4: Downloading from web and other installations](Chapters/Chapter4-WebDownloadAndInstallations.md)
5. [Chapter 5: Run Jenkins in Background](Chapters/Chapter5-RunJenkinsBackground.md)
6. [Chapter 6: Display and kill active services](Chapters/Chapter6-Process.md)
7. [Chapter 7: Cronjob fundamentals](Chapters/Chapter7-CronjobFundamentals.md)
8. [Chapter 8: Creating crom jobs](Chapters/Chapter8-SchedulingCronjob.md)

## Contact Me
- In the corresponding repository >> go to Plan >> Issues >> raise a New Issue
- inwincibleyou@gmail.com
- 8928885292

## Website
https://inwincibleshashank.in/