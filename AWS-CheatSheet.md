# Cheat Sheet
For quick interview preparation, use these summarized commands

## Lessons
For in-depth practical learnings, learn from [Chapters](README.md#quick-start) created.

## Linux cmds
On ubuntu >> linux O.S. of AWS EC2 server for various linux cmds use this - https://gitlab.com/inwincible_shashank/linuxautomationcmds/-/blob/main/LinuxCmdCheatSheet.md

### Connecting to EC2 instance using Gitbash
- `chmod 400 "inwincible-prod-keypair.pem"`: to grant necessary permissions
- `ssh -i "inwincible-prod-keypair.pem" ubuntu@ec2-35-154-47-126.ap-south-1.compute.amazonaws.com`: 
    - ssh (secure shell) as a cmd
    - -i ".pem key on local system"
    - username given by AWS EC2 instance
    - @iPv4 address
    - EC2 instance location
    - AWS url: compute.amazonaws.com *(EC2 service belongs to compute)*

### Connecting to EC2 instance using MobaXterm
MobaXterm >> Session >> SSH >> Basic/Advance ssh setting
- `ec2-35-154-47-126.ap-south-1.compute.amazonaws.com`: get this value from EC2 instance dashboard and add against 'Remote host'
- ubuntu: add username value given by EC2 instance
- Browse .pem private key from local and session will be created on MobaXterm

### Install latest apt
```
sudo apt update
```
- this cmd to be used when new instance/server is created
- sudo is for admin privileges
- apt: advance packaging tool

### Install java
```
sudo apt install openjdk-11-jre-headless
java -version
```

### Download and install jenkins
```
wget https://get.jenkins.io/war-stable/2.452.2/jenkins.war
java -jar jenkins.war
```
- On AWS EC2 instance will update security >> Security groups >> Inbound rules >> Edit inbound rules
- Add rule: TCP with port 8080

### Run Jenkins in Background
- Create shell file 'launchJenkins.sh' with content "java -jar jenkins.war"
```
nohup sh launchJenkins.sh &
```
- nohup is no hungup
- sh cmd is used to pass on .sh file
- & indicates it is a background process

### Display Active Services/ Stop Active Service
```
ps -e : returns all active services by user, system O.S. on EC2 instance
```
```
ps -ef : returns all active services by user, system O.S. on EC2 instance in human readable format
```
```
kill XXXX: stops the process (where XXXX is the processId)
```

### Cronjob
```
crontab -e : Edit Crontab
```
```
crontab -l : List Crontab
```
```
crontab -r : Remove Crontab
```
```
sudo crontab -e -u username : Edit Crontab for a Specific User
```

### Schedule cronjob
    * minute(0-59)
    * hour(0-23)
    * dayOfMonth(1-31)
    * month(1-12)
    * dayOfWeek(0-6)     
```
crontab -e : this will open file editor to add cronjob instructions
```
30 19 * * * cp /home/ubuntu/linuxautomatiocmds/Website.log /home/ubuntu/linuxautomationcmds/backupWebsite.log