## Run Jenkins in Background

Scenario: When we launch jenkins using cmd through terminal we cannot use terminal for any further activities as [jenkins is up and running](/Chapters/Chapter4-WebDownloadAndInstallations.md/) >> Scenario: Launch Jenkins

Solution: We need a process in which jenkins can run in background and we can further use our terminal for other activities

Steps:
1. Create a shell file 'launchJenkins.sh'
2. Edit file, add content "java -jar jenkins.war" and save file
3. Then to run jenkins in the background with out any hungup use below cmd:
```
nohup sh launchJenkins.sh &
```
*(the sign '&' indicates it is a background process only)*

4. Now access Ipv4 address link with 8080 port jenkins will work

## Reference screenshots
![jenkins background Screenshot](../Screenshot/nohup.png)


