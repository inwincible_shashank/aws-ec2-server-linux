## Default Directories and Their Uses

1. / (Root Directory):
The top-level directory of the file system. All other directories are subdirectories of the root.

2. /bin:
Contains essential command binaries (executables) that are required for the system to boot and run.

3. /boot:
Contains the kernel and other files needed to boot the system.

4. /dev:
Contains device files. These files represent hardware components and can be used to interact with hardware directly.

5. /etc:
Contains configuration files for the system and installed applications. For example, /etc/hostname and /etc/hosts.

6. /home:
Contains personal directories for all users. Each user has a subdirectory within /home (e.g., /home/ubuntu).

7. /lib:
Contains shared library files required by the binaries in /bin and /sbin.

8. /media:
Used for mounting removable media such as CD-ROMs and USB drives.

9. /mnt:
Used for temporarily mounting filesystems.

10. /opt:
Contains optional software and add-on packages that are not part of the default system installation.

11. /proc:
A virtual filesystem that contains information about system processes and hardware. This is a pseudo-filesystem that provides an interface to kernel data structures.

12. /root:
The home directory of the root user.

13. /run:
Contains runtime data for processes started since the last boot.

14. /sbin:
Contains essential system binaries required for system administration.

15. /srv:
Contains data for services provided by the system, such as web and FTP servers.

16. /tmp:
Contains temporary files created by system processes and users. This directory is typically cleared upon system reboot.

17. /usr:
Contains user-installed software and libraries. This is often the largest directory and includes subdirectories like /usr/bin, /usr/lib, and /usr/share.

18. /var:
Contains variable data files, such as logs (/var/log), mail, and databases. This directory's contents often change dynamically as the system runs.

```
whoami
```
```
pwd
```
```
df -h
```
```
lsblk
```

---
## Reference screenshots:
![Linux Dir Screenshot](../Screenshot/LinuxDir.png)