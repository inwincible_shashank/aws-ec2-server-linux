## Cronjob/ Crontab Fundamentals
- Used to schedule tasks to be executed automatically at specified intervals on Unix-like operating systems. These tasks are known as "cron jobs" 
- Crontab stands for "cron table" because it uses a table to define the scheduling of jobs.

```
crontab -e : Edit Crontab
```
```
crontab -l : List Crontab
```
```
crontab -r : Remove Crontab
```
```
sudo crontab -e -u username : Edit Crontab for a Specific User (used sudo for admin privileges)
```

## Example of job definition
- minute (0 - 59)
- hour (0 - 23)
- day of month (1 - 31)
- month (1 - 12) OR jan,feb,mar,apr ...
- day of week (0 - 6) (Sunday=0 or 7)

```
*  *  *  *  * user-name  command to be executed
```

1. Run backup.sh at 2:00 AM every day
0 2 * * * /home/ubuntu/backup.sh

2. Run cleanup.sh at midnight on the 1st of every month
0 0 1 * * /home/ubuntu/cleanup.sh

3. Run script.sh every 5 minutes
*/5 * * * * /home/ubuntu/script.sh

4. Run weekly_report.sh at 6:30 AM every Monday
30 6 * * 1 /home/ubuntu/weekly_report.sh

5. Run job.sh at 3:00 PM on the 15th of every month
0 15 15 * * /home/ubuntu/job.sh