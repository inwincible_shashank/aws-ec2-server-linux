## Downloading From Web using 'wget'
Scenario: Download jenkins.war file on EC2 server using mobaXterm

1. Launch mobaXterm and create new folder 'automationSoft'
2. In that folder will execute below cmd *(wget: world wide web get)*
```
wget https://get.jenkins.io/war-stable/2.452.2/jenkins.war
```

## Install latest apt
```
sudo apt update
```
- this cmd to be used when new instance/server is created
- sudo is for admin privileges
- apt: advance packaging tool 

## Installation using 
Scenario: Install 'java-jdk' on EC2 server using mobaXterm

1. Type 'java' on terminal which should state cmd not found
2. And should state list of available java-jdk versions
3. Should also provide cmd to install them
4. So select the cmd and click to paste 
5. Install the required version
```
sudo apt install openjdk-11-jre-headless
```
```
java -version
```

Scenario: Launch jenkins
1. Make sure you are in a dir where 'jenkins.war' file is present
2. Execute below cmd
```
java -jar jenkins.war
```
3. During installation for first time we will be asked to complete a intial setup at browser level - "Jenkins initial setup is required. An admin user has been created and a password generated."
#### P.S. when we created EC2 instance the only port allowed was 22 for ssh cmd so that we can connect to server. But to launch jenkins we need to allow port 8080
#### e.g. server=home and ports=doors. if doors are closed it is secure when more doors are open it is less secure and vulnerable 
4. So on AWS EC2 instance will update security >> Security groups >> Inbound rules >> Edit inbound rules
5. Add rule: TCP with port 8080 and save changes
6. Copy public Ipv4 address and add jenkins port 8080 in browser
http://35.154.47.126:8080/ you should see 'Unlock Jenkins' screen with Admin Password
7. Copy admin password from terminal, paste and continue
8. Select install all suggested plugins
9. Once plugins are installed setup your admin username and password


---
## Reference screenshots
![wget Screenshot](../Screenshot/wget1.png)
![apt Screenshot](../Screenshot/apt1.png)
![apt Screenshot](../Screenshot/apt2.png)
![java jenkins Screenshot](../Screenshot/javajenkins1.png)
![java jenkins Screenshot](../Screenshot/javajenkins2.png)
![java jenkins Screenshot](../Screenshot/javajenkins3.png)
![java jenkins Screenshot](../Screenshot/javajenkins4.png)
![java jenkins Screenshot](../Screenshot/javajenkins5.png)
![java jenkins Screenshot](../Screenshot/javajenkins6.png)
![java jenkins Screenshot](../Screenshot/javajenkins7.png)
![java jenkins Screenshot](../Screenshot/javajenkins8.png)
![java jenkins Screenshot](../Screenshot/javajenkins9.png)
![java jenkins Screenshot](../Screenshot/javajenkins10.png)