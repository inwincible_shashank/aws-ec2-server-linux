## Scheduling Cronjob

### Pre-requisite: 
- Connect to EC2 instance using mobaXterm
- Execute cmd: "mkdir linuxautomationcmds" and it will be created inside dir ;'home/ubuntu'
- Inside 'home/ubuntu/linuxautomationcmds' drag and drop local files

### Scenario 1: In the given folder create a copy of website log everyday once named as 'backupWebsite.log'
- Navigate to cd /bin
- To add cronjob instructions use below cmd:
```
crontab -e : this will open file editor to add cronjob instructions
```
- Add cronjob instructions: 30 19 * * * cp /home/ubuntu/linuxautomatiocmds/Website.log /home/ubuntu/linuxautomationcmds/backupWebsite.log

### Scenario 2: Delete the file 'backupWebsite.log' after it's creation in the next 12 hours

## Reference screenshots
![Upload local files Screenshot](../Screenshot/uploadFiles.png)
![CronScenario1 files Screenshot](../Screenshot/cronScenario1.png)
![CronScenario1 files Screenshot](../Screenshot/cronScenarioError.png)
![CronScenario1 files Screenshot](../Screenshot/cronScenario2.png)