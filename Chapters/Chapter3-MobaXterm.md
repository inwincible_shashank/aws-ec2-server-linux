*(to avoid connecting to EC2 with ssh command evrytime from local windows will perform below one time process)*

## MobaXterm Installation
1. Download home edition - https://mobaxterm.mobatek.net/download-home-edition.html
2. Install and open 
3. From header click on 'Session' >> 'SSH'

## Basic SSH settings
1. Remote host = ec2-35-154-47-126.ap-south-1.compute.amazonaws.com *(copy from EC2 instance >> Public IPv4 DNS)*
2. Specify username = ubuntu 
3. port (default) = 22

## Advance SSH seting
1. Checkbox 'Use Private key' and locate .pem file from local
2. Then click on 'Ok' and 'Accept'
3. On mobaXterm terminal 'EC2 instance' should be launched and displayed

## Exit and Launch EC2 server using MobaXterm
1. When typed cmd 'exit' EC2 Instance session should stop and mobaXterm will be closed
2. When we want to connect to EC2 server next time just open and click on the server name pinned on left panel. 

---
## Reference screenshots
![Mobaxterm Screenshot](../Screenshot/Mobaxterm1.png)
![Mobaxterm Screenshot](../Screenshot/Mobaxterm2.png)
![Mobaxterm Screenshot](../Screenshot/Mobaxterm3.png)
![Mobaxterm Screenshot](../Screenshot/Mobaxterm4.png)
![Mobaxterm Screenshot](../Screenshot/Mobaxterm5.png)