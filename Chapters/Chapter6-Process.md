## Display Active Services/Process
```
ps : returns all the active backgroud services by user on EC2 instance
```
```
ps -e : returns all active services by user and by O.S. on EC2 instance
```
```
ps -ef : returns all active services by user and by O.S. on EC2 instance in human readable format (here we can see user and it's respective activated service)
```

## Kill Active Service/Process
- kill cmd is used to stop active service by providing process id
- To get the process id execute command 'ps -ef'
- Once child process is stopped then it will stop the parent process as well
```
kill XXXX: stops the process (where XXXX is the processId)
```

## Reference screenshots
![Background process Screenshot](../Screenshot/pscmd.png)
![Stop process Screenshot](../Screenshot/killcmd.png)
![Stop process Screenshot](../Screenshot/killcmd1.png)

