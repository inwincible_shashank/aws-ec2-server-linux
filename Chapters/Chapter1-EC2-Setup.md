#### [For ease of understanding refer screenshot provided for each step]

## EC2 Setup
1. Login to AWS console as a root user
2. Under header select : Services >> Compute >> EC2
3. Then click on 'Launch Instance'
4. Provide 'Name' to the server >> select O.S. (Ubuntu/Linux)
5. For AMI select the Free and latest version available *(in future when you are creating this server ensure you select Free tier and latest AMI)*
6. Select architecture 64-bit (x86)
7. Select Instance type *(Hardware configuration)* 't2.micro' *(it is free tier)*
8. Create a new key-pair to establish a secure connection to the server from local machine *(keep this keypair safe once deleted we won't be able to recover again)*
9. Keep network setting as it is
10. Keep hardware storage as 8 gb out of available 30 gb

## EC2 launch sequence
1. Click on Launch Instance and you should see Success message on redirected page with unique identification no. of the server
2. Clicking on the server number should redirect to Instance dashboard with details
3. On that dashboard select server and click on connect
4. Use 'SSH client' tab and follow given cmds

## Connecting to EC2 instance
1. Open gitbash folder where .pem file is stored
2. To grant necessary permission copy and execute cmd: chmod 400 "inwincible-prod-keypair.pem"
 - Before executing below cmd the path on gitbash is local (/d/AUTOMATION/DAILY GIT PUSH 2024/PEM) 
3. To connect to server from local use this cmd: ssh -i "inwincible-prod-keypair.pem" ubuntu@ec2-35-154-47-126.ap-south-1.compute.amazonaws.com
*(please note in your case based on names above will change)*
- After connecting to linux server path on gitbash is of server (ubuntu@ip-172-31-36-202:~$)

---
## Reference screenshots:
![EC2 service Screenshot](../Screenshot/SelectEc2Server.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance1.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance2.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance3.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance4.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance5.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance6.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance7.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance8.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance9.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance10.png)
![EC2 service Screenshot](../Screenshot/LaunchEC2Instance11.png)



